require 'rails_helper'

RSpec.describe "books/edit", type: :view do
  before(:each) do
    @book = assign(:book, Book.create!(
      :reference => "MyString",
      :title => "MyString",
      :author => "MyString",
      :synopsis => "MyText",
      :cover => "MyString"
    ))
  end

  it "renders the edit book form" do
    render

    assert_select "form[action=?][method=?]", book_path(@book), "post" do

      assert_select "input[name=?]", "book[reference]"

      assert_select "input[name=?]", "book[title]"

      assert_select "input[name=?]", "book[author]"

      assert_select "textarea[name=?]", "book[synopsis]"

      assert_select "input[name=?]", "book[cover]"
    end
  end
end
