require 'rails_helper'

RSpec.describe "books/index", type: :view do
  before(:each) do
    assign(:books, [
      Book.create!(
        :reference => "Reference",
        :title => "Title",
        :author => "Author",
        :synopsis => "MyText",
        :cover => "Cover"
      ),
      Book.create!(
        :reference => "Reference",
        :title => "Title",
        :author => "Author",
        :synopsis => "MyText",
        :cover => "Cover"
      )
    ])
  end

  it "renders a list of books" do
    render
    assert_select "tr>td", :text => "Reference".to_s, :count => 2
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "Author".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Cover".to_s, :count => 2
  end
end
