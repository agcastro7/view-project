require 'rails_helper'

RSpec.describe "books/new", type: :view do
  before(:each) do
    assign(:book, Book.new(
      :reference => "MyString",
      :title => "MyString",
      :author => "MyString",
      :synopsis => "MyText",
      :cover => "MyString"
    ))
  end

  it "renders new book form" do
    render

    assert_select "form[action=?][method=?]", books_path, "post" do

      assert_select "input[name=?]", "book[reference]"

      assert_select "input[name=?]", "book[title]"

      assert_select "input[name=?]", "book[author]"

      assert_select "textarea[name=?]", "book[synopsis]"

      assert_select "input[name=?]", "book[cover]"
    end
  end
end
