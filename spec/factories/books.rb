# == Schema Information
#
# Table name: books
#
#  id         :integer          not null, primary key
#  reference  :string
#  title      :string
#  author     :string
#  synopsis   :text
#  cover      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryBot.define do
  factory :book do
    reference { "MyString" }
    title { "MyString" }
    author { "MyString" }
    synopsis { "MyText" }
    cover { "MyString" }
  end
end
