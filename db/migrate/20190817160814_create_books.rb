class CreateBooks < ActiveRecord::Migration[5.2]
  def change
    create_table :books do |t|
      t.string :reference
      t.string :title
      t.string :author
      t.text :synopsis
      t.string :cover

      t.timestamps
    end
  end
end
