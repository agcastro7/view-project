Rails.application.routes.draw do
  resources :books
  get '/last', to: 'books#last', as: :last_book
  get '/search', to: 'books#search', as: :search_book
  root to: 'books#index'
end
