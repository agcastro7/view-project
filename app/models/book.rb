# == Schema Information
#
# Table name: books
#
#  id         :integer          not null, primary key
#  reference  :string
#  title      :string
#  author     :string
#  synopsis   :text
#  cover      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Book < ApplicationRecord
  validates :reference, presence: :true
  validates :title, presence: :true
  validates :author, presence: :true
  validates :synopsis, presence: :true
  validates :cover, presence: :true
end
