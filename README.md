# Controller exercises

To download this code, execute in bash:

```bash
git clone git@bitbucket.org:agcastro7/view-project.git
cd view-project
bundle
rails db:migrate
rails s
```
